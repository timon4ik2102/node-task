const { fighter } = require('../models/fighter');
const { alertInformationText } = require('./validators/alertInformation');
const ValidatorService = require('./validators/validatorService');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { errors, isValid } = validateFighterInput(req.body);
    console.log(req.body);

    if (!isValid) {
        return res.status(400).json({
            error: true,
            message: alertInformationText(errors),
        });
    }

    next();
};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const { errors, isValid } = validateFighterInput(req.body);

    if (!ValidatorService.fighterExists({ id: req.params.id })) {
        return res.status(404).send({
            error: true,
            message: 'Fighter not found',
        });
    }

    if (!isValid) {
        return res.status(400).json({
            error: true,
            message: alertInformationText(errors),
        });
    }

    next();
};

const validateFighterInput = (data) => {
    let errors = {};

    data.name = data.name ? data.name : '';
    data.health = data.health ? data.health : '';
    data.power = data.power ? data.power.toString() : '';
    data.defense = data.defense ? data.defense.toString() : '';

    if (!ValidatorService.numberOfFields(data, 4)) {
        errors.fields = 'All fields must be filled in';
    }

    if (!data.name) {
        errors.name = 'Please, enter a fighter name';
    }

    if (!data.health) {
        errors.health = 'Please, enter a number of health';
    } else if (!ValidatorService.isSmaller(data.health, 11) || !ValidatorService.isBigger(data.health, 101)) {
        errors.health = 'Health number is incorrect. Please enter number between 11 and 100';
    }

    if (!data.power) {
        errors.power = 'Please, enter a number of power';
    } else if (!ValidatorService.isSmaller(data.power, 1) || !ValidatorService.isBigger(data.power, 11)) {
        errors.power = 'Power number is incorrect. Please enter number between 1 and 10';
    }

    if (!data.defense) {
        errors.defense = 'Please, enter a number of defence';
    } else if (!ValidatorService.isSmaller(data.defense, 1) || !ValidatorService.isBigger(data.defense, 11)) {
        errors.defense = 'Defense number is incorrenct. Please enter number between 1 and 10';
    }

    function isEmptyObj(someobj) {
        return Object.keys(someobj).length == 0 ? true : false;
    }

    return {
        errors,
        isValid: isEmptyObj(errors),
    };
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
