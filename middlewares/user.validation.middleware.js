const { user } = require('../models/user');

const { alertInformationText } = require('./validators/alertInformation');
const ValidatorService = require('./validators/validatorService');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    // TODO: Implement validation for user entity during creation

    const { errors, isValid } = validateUserInput(req.body);

    if (!isValid) {
        return res.status(400).json({
            error: true,
            message: alertInformationText(errors),
        });
    }

    next();
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validation for user entity during update

    const { errors, isValid } = validateUserInput(req.body);

    if (!ValidatorService.userExists({ id: req.params.id })) {
        return res.status(404).send({
            error: true,
            message: 'User not found',
        });
    }

    if (!isValid) {
        return res.status(400).json({
            error: true,
            message: alertInformationText(errors),
        });
    }

    next();
};

const validateUserInput = (data) => {
    let errors = {};

    data.firstName = data.firstName ? data.firstName : '';
    data.lastName = data.lastName ? data.lastName : '';
    data.email = data.email ? data.email : '';
    data.phoneNumber = data.phoneNumber ? data.phoneNumber : '';
    data.password = data.password ? data.password : '';

    if (!ValidatorService.numberOfFields(data, 5)) {
        errors.fields = 'All fields must be filled in';
    }

    if (!data.firstName) errors.firstName = 'Please, enter first name';
    if (!data.lastName) errors.lastName = 'Please, enter last name';
    if (!data.email) errors.email = 'Please, enter your email';
    if (!data.phoneNumber) errors.phoneNumber = 'Please, enter your phone';
    if (!ValidatorService.isGmailEmail(data.email)) {
        errors.email = 'Email is invalid. Only gmail';
    } else if (!ValidatorService.uniqueEmail(data.email)) {
        errors.email = 'The email is already registered. Please sign in.';
    }

    if (!ValidatorService.isPhoneNumber(data.phoneNumber)) {
        errors.phoneNumber = 'Phone number must have format: +380xxxxxxxxx';
    }

    if (!ValidatorService.isLength(data.password, { min: 3 })) {
        errors.password = 'password must have at least 3  characters';
    }

    function isEmptyObj(someobj) {
        return Object.keys(someobj).length == 0 ? true : false;
    }

    return {
        errors,
        isValid: isEmptyObj(errors),
    };
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
