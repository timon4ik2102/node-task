const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.err) {
        if (res.err === 'User not found') {
            return res.status(404).json({
                error: true,
                message: res.err,
            });
        }
    }
    if (res.data) {
        res.statusCode = 200;
        res.json(res.data);
    }
    next();
};

exports.responseMiddleware = responseMiddleware;
