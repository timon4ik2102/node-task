const alertInformationText = (obj) => {
    let str = '';
    for (let key in obj) {
        str += `${obj[key]}\n`;
    }
    return str;
};

exports.alertInformationText = alertInformationText;
