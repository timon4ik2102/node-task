const { user } = require('../models/user');

const { alertInformationText } = require('./validators/alertInformation');
const ValidatorService = require('./validators/ValidatorService');
const UserService = require('../services/userService');
const userSchema = Object.assign({}, user);

const loginUserValid = (req, res, next) => {
    const { errors, isValid } = validateLoginInput(req.body);

    if (!isValid) {
        return res.status(400).json({
            error: true,
            message: alertInformationText(errors),
        });
    }

    next();
};

const validateLoginInput = (data) => {
    let errors = {};

    data.email = data.email ? data.email : '';
    data.password = data.password ? data.password : '';

    if (!data.email) errors.email = 'Please, enter your email';
    if (!data.password) errors.password = 'Please, enter your password';

    if (!ValidatorService.isGmailEmail(data.email)) {
        errors.email = 'Email is invalid. Only gmail';
    }

    if (!ValidatorService.numberOfFields(data, 2)) {
        errors.fields = 'Some fields are empty';
    }
    function isEmptyObj(someobj) {
        return Object.keys(someobj).length == 0 ? true : false;
    }
    return {
        errors,
        isValid: isEmptyObj(errors),
    };
};

exports.loginUserValid = loginUserValid;
