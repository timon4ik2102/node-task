class Fight {
    constructor({ userId, winner, loser }) {
        this.userId = userId;
        this.winner = winner;
        this.loser = loser;
        this.date = new Date();
    }
}

module.exports.Fight = Fight;
